# Blau Araujo - cursos livres

## Conteúdos programáticos dos nossos cursos

- [Programação do Bash](cursos/cpb.md)
- [Expressões Regulares no Shell do GNU/Linux](cursos/ers.md)
- Interfaces para scripts em shell
- Programação em AWK
- Shell GNU/Linux Prático

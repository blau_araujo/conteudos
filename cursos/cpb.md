# Programação do Bash

- **Carga horária média:** 40 horas
- **Total de aulas:** 20 aulas

## Conteúdo

A ordem e o aprofundamento dos tópicos podem variar de acordo com o desenvolvimento das aulas.

### Tópico 1: Processos e sessões do shell

- Relação do shell com o terminal
- Fluxos de dados
- O que são processos
- Chamadas de sistema `fork` e `exec`
- Invocação do shell
- O que são sessões
- A *shebang*
- Parâmetros e variáveis
- O conceito de expansão
- Sessões mães, filhas e subshells

### Tópico 2: Como o shell processa os comandos

- O que é uma linha de comando
- Etapas de processamento de comandos
- Separação de palavras e regras de citação
- Expansão de apelidos
- Comandos internos
- Comandos simples, complexos e compostos
- Operadores de controle
- Estados de saída
- Listas de comandos e pipelines

### Tópico 3: As estruturas do shell

- Comandos compostos
- Agrupamentos com chaves e parêntesis
- Avaliação de expressões assertivas
- Avaliação de expressões lógicas e aritméticas
- Estruturas de decisão
- Estruturas de repetição
- Funções

### Tópico 4: Expansões de parâmetros

- Ordem de processamento das expansões
- Variáveis vetoriais e escalares
- Parâmetros especiais do shell
- Sintaxe da expansão de parâmetros
- Quantidades de caracteres e elementos
- Indireções
- Expansão de faixas de caracteres ou de elementos
- Aparagem de strings
- Substituições de strings
- Alterando a caixa do texto
- Expansões condicionais

### Tópico 5: Outras expansões

- Expansão das chaves
- Expansão do til
- Substituições de comandos
- Substituições de processos
- Expansões aritméticas
- Separação de palavras expandidas
- Expansões de nomes de arquivos
- Remoção das citações
- Expansão de operadores
- Expansão de caracteres de controle

### Tópico 6: Redirecionamentos

- Descritores de arquivos
- Fluxos de dados padrão
- Operadores de redirecionamento
- Leitura de arquivos
- Escrita em arquivos
- Duplicação de descritores de arquivos

### Tópico 7: Pipes

- Fluxos de dados entre processos
- Operadores de pipe
- Arquivos FIFO
- Escopo de variáveis em pipelines
- Pipes nomeados
- *Here-documents* e *here-strings*

### Tópico 8: Ferramentas auxiliares

- Validação de código (`shellcheck`)
- GNU Coreutils
- Introdução às expressões regulares
- Manipulação de fluxos de texto
- Busca e substituição

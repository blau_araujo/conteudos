# Expressões Regulares no Shell do GNU/Linux

- **Carga horária média:** 18 horas
- **Quantitdade de aulas:** 9 aulas

## Conteúdo

A ordem e o aprofundamento dos tópicos pode variar de acordo com o desenvolvimento das aulas.

### Casamento de padrões no shell

- O que são padrões
- Estruturas, comandos e ferramentas
- Métodos de formação de padrões
- File name pattern matching (FNPM)

### Expressões regulares

- Tipos de expresões regulares
- Metacaracteres
- Classes nomeadas

### Grupos e registradores

- Elementos alternativos
- Precedência de agrupamentos
- Captura de referências prévias

### Casamentos gulosos (greedy)

- Técnicas com expressões regulares estendidas
- Técnicas com quantificadores dobrados

### Operadores PCRE

- Comentários
- Grupos não capturados
- Asserções positivas e negativas
- Descarte do início do casamento

### Expressões regulares no Bash

- O comando composto `[[`
- A variável BASH_REMATCH

### Ferramentas

- Filtrando linhas de texto com grep
- Editando fluxos de texto com sed
- Processando textos com awk
- Localizando arquivos com find
- Busca e substituição com perl

### Casos de estudo

- Extração de dados em HTML
- Validação de entradas de dados
- Validação dados numéricos
- Conversão de markdown para sequências de escape
- Captura de informações do sistema
